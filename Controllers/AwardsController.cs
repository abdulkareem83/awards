﻿using BusinessLogic.Abstracts;
using BusinessLogic.Localization;
using BusinessLogic.Models;
using System.Web.Mvc;

namespace AwardsApp.Controllers
{
    [Authorize]
    public class AwardsController : Controller
    {
        private readonly IAwardService _awardsService;
        private readonly IDraughtService _draughtService;

        public AwardsController(IAwardService awardService, IDraughtService draughtService)
        {
            _awardsService = awardService;
            _draughtService = draughtService;
        }

        /// <summary>
        /// Show the awards page
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            // get all awrds
            var awards = _awardsService.GetAwards();
            ViewBag.Alert = TempData["Alert"];

            return View(awards);

        }

        /// <summary>
        /// Create a ward
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Create(long? id)
        {
            var award = new AwardVm
            {
                Order = _awardsService.GetNextOrder()
            };
            if (id != null)
            {
                award = _awardsService.GetAwardVm((long)id);
                ViewBag.Draughts = new SelectList(_draughtService.GetDraughts(), "Id", "Name", award != null ? award.DraughtId : 0);
                if (award == null)
                {
                    ViewBag.Alert = new ResultVm { Status = false, Message = ArabicResources.InvalidData };
                    return View();
                }
                return View(award);
            }

            ViewBag.Draughts = new SelectList(_draughtService.GetDraughts(), "Id", "Name");
            ViewBag.Alert = TempData["Alert"];
            return View(award);
        }

        public ActionResult Save(AwardVm award)
        {
            if (!ModelState.IsValid)
                return View("Create", award);

            var result = _awardsService.SaveAward(award);

            if (!result.Status)
            {
                ViewBag.Alert = result;
                ViewBag.Draughts = new SelectList(_draughtService.GetDraughts(), "Id", "Name");
                return View("Create", award);
            }


            TempData["Alert"] = result;
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Delete(long id)
        {
            TempData["Alert"] = _awardsService.DeleteAward(id);
            return RedirectToAction("Index");
        }
    }
}