﻿using BusinessLogic.Abstracts;
using BusinessLogic.Localization;
using BusinessLogic.Models;
using BusinessLogic.ViewModels;
using System.Web.Mvc;

namespace AwardsApp.Controllers
{
    [Authorize]
    public class DraughtController : Controller
    {
        #region Fields
        private readonly IDraughtService _draughtService;
        #endregion

        public DraughtController(IDraughtService draughtService)
        {
            _draughtService = draughtService;
        }


        #region Mvc Actions
        /// <summary>
        /// Show all draughts
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            // get all draughts
            var draughts = _draughtService.GetDraughts();
            ViewBag.Alert = TempData["Alert"];

            return View(draughts);
        }

        public ActionResult Create(long? id)
        {
            var draught = new DraughtVm();

            if (id != null)
            {
                draught = _draughtService.GetDraughtVm((long)id);
                if (draught == null)
                {
                    ViewBag.Alert = new ResultVm { Status = false, Message = ArabicResources.InvalidData };
                    return View();
                }
            }
            ViewBag.Alert = TempData["Alert"];
            return View(draught);
        }
        public ActionResult Save(DraughtVm draught)
        {
            if (draught.Id == 0 && draught.BackgroundFile == null)
                ModelState.AddModelError("BackgroundFile", ArabicResources.ImageIsRequired);

            if (!ModelState.IsValid)
                return View("Create", draught);

            var result = _draughtService.SaveDraught(draught);

            if (!result.Status)
            {
                ViewBag.Alert = result;
                return View("Create", draught);
            }


            TempData["Alert"] = result;
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Delete(long id)
        {
            TempData["Alert"] = _draughtService.DeleteDraught(id);
            return RedirectToAction("Index");
        }

        public ActionResult CounterSpeed()
        {
            ViewBag.Alert = TempData["Alert"];
            return View(_draughtService.GetCounterSpeed());
        }

        [HttpPost]
        public ActionResult CounterSpeed(CounterSpeedVm counterSpeedVm)
        {
            if (!ModelState.IsValid)
                return RedirectToAction("CounterSpeed", counterSpeedVm);

            TempData["Alert"] = _draughtService.SaveCounterSpeed(counterSpeedVm);
            return RedirectToAction("CounterSpeed");
        }
        #endregion

    }
}