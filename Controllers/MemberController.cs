﻿using BusinessLogic.Abstracts;
using BusinessLogic.ViewModels;
using System.Net.Mime;
using System.Web;
using System.Web.Mvc;

namespace AwardsApp.Controllers
{
    [Authorize]
    public class MemberController : Controller
    {
        private readonly IUserService _userService;

        public MemberController(IUserService userService)
        {
            _userService = userService;
        }

        public ActionResult Winners()
        {
            var winners = _userService.GetWinners();

            return View(winners);
        }

        public ActionResult ExportWinnersFile()
        {
            _userService.WriteWinnerIntoFile();
            byte[] fileBytes = System.IO.File.ReadAllBytes(Server.MapPath("/Data/winners.txt"));
            string fileName = "winners.txt";
            return File(fileBytes, MediaTypeNames.Text.Plain, fileName);
        }

        public ActionResult ImportUsers()
        {
            ViewBag.UsersCount = _userService.GetUsersCount();
            if (TempData["Alert"] != null)
                ViewBag.Alert = TempData["Alert"];

            return View();
        }
        [HttpPost]
        public JsonResult ImportUsers(HttpPostedFileBase file)
        {
            return Json(_userService.ImportUsers(file), JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteAllUsers()
        {
            TempData["Alert"] = _userService.DeleteAllUsers();

            return RedirectToAction("ImportUsers");
        }

        public JsonResult GetUsersCount()
        {
            var count = new UsersCountVm
            {
                AllUsersCountIntheFile = _userService.GetAllUsersInFileCount(),
                ImportedUsers = _userService.GetImportedUsersCount()
            };

            return Json(count, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveUsers()
        {
            _userService.SaveUsers();
            return View();
        }
    }
}