﻿using BusinessLogic.Abstracts;
using BusinessLogic.Localization;
using BusinessLogic.Models;
using System.Web.Mvc;

namespace AwardsApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly IUserService _userService;
        private readonly IDraughtService _draughtService;

        public HomeController(IUserService userService, IDraughtService draughtService)
        {
            _userService = userService;
            _draughtService = draughtService;
        }

        /// <summary>
        /// Show the awards page
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            // get the awards with the winners if there is
            var awards = _userService.GetAwardsWithWinners();

            ViewBag.CounterSpeed = _draughtService.GetCounterSpeed();
            return View(awards);
        }

        public JsonResult SelectUser(int? order, long? awardId)
        {
            if (!order.HasValue || order.Value < 1 || !awardId.HasValue)
                return Json(new ResultVm { Status = false, Message = ArabicResources.InvalidData });

            return Json(_userService.SelectUser((int)order, (long)awardId), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetCurrentBackGround()
        {
            return Content(_draughtService.GetCurrentBackground());
        }
    }
}