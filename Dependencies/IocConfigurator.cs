﻿using Autofac;
using Autofac.Integration.Mvc;
using AwardsApp;
using BusinessLogic.Abstracts;
using BusinessLogic.Services;
using MsdnMag;
using System.Web.Mvc;

namespace SEA.Hajj.Portal.DependencyInjection
{
    public static class IocConfigurator
    {
        public static void ConfigureDependencyInjection()
        {
            var builder = new ContainerBuilder();

            builder.RegisterControllers(typeof(MvcApplication).Assembly);

            // register services
            builder.RegisterGeneric(typeof(DataImportFromExcel<>)).As(typeof(IDataImportFromExcelFile<>));
            builder.RegisterType<CryptoRandom>().As<CryptoRandom>();
            builder.RegisterType<AwardService>().As<IAwardService>();
            builder.RegisterType<DraughtService>().As<IDraughtService>();
            builder.RegisterType<UserService>().As<IUserService>();

            IContainer container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}