﻿var counterSpeed;
var AwardsScript = {
    nextOrder: 0,
    counterOptions: {},

    init: function () {
        $(document).ready(function () {
            AwardsScript._setUnWardedNextOrder();
            AwardsScript._initCounter();
            AwardsScript._onSelectUserClick();
            AwardsScript._setTheAwardName();
            AwardsScript._validateLastAward();
            AwardsScript._hideLoader();
            AwardsScript.counterOptions["duration"] = counterSpeed;
        })
    },

    _GetCurrentUsers: function () {
        return setInterval(function () {
            // get the curent users
            $.get("/member/GetUsersCount")
                .done(function (result) {
                    if (result !== undefined) {
                        $(".users-count-section .count").text(result.ImportedUsers);
                        $(".users-count-section .total").text(result.AllUsersCountIntheFile);
                    }
                });

        }, 20000);
    },

    OnSaveUsersClick: function () {
        $(".users-section").on("click", ".save-users", function () {
            var interval = AwardsScript._GetCurrentUsers();
            AwardsScript._showLoader();

            // show the overlay with numbers
            $(".spinner").addClass("top-abit")
                .after($(".users-count-section").removeClass("hidden"));
            
            
            $("#users-sheet").simpleUpload("/member/importusers", {
                success: function (result) {
                    // show the overlay with numbers
                    $(".spinner").removeClass("top-abit");
                    $(".users-count-section").remove();

                    AwardsScript._hideLoader();

                    toastr["success"](result.Message);
                    clearInterval(interval);
                },

                progress: function (progress) {
                    //received progress
                    $(".file-progress .number").text(progress);
                    if (parseInt(progress) === 100)
                        $(".file-progress").addClass("hidden");
                    
                },
            });
        });
    },

    OnDeleteClick: function () {
        $(".awards-section, .users-section").on("click", ".delete", function () {
            var $deleteForm = $(this).parents(".parent")
                .first().find("form"); 

            bootbox.confirm({
                message: "تأكيد الحذف",
                buttons: {
                    confirm: {
                        label: 'حذف',
                        className: 'btn-danger'
                    },
                    cancel: {
                        label: 'الغاء',
                        className: 'btn btn-default'
                    }
                },
                callback: function (result) {
                    if (result) {
                        // submit delete form
                        $deleteForm.submit();
                    }
                }
            });
        });
    },

    OnPrintClick: function () {
        $(".awards-section").on("click", "#Print", function () {
            var headstr = "<html><head><title></title></head><body>";
            var footstr = "</body>";
            var newstr = document.all.item('Printable').innerHTML;
            var oldstr = document.body.innerHTML;
            document.body.innerHTML = headstr + newstr + footstr;
            window.print();
            document.body.innerHTML = oldstr;
        });

    },

    _initCounter: function () {
        $(".incremental-counter").incrementalCounter(AwardsScript.counterOptions);
    },

    _onSelectUserClick: function () {
        $(".awards-page").on("click", ".select-user", function () {

            // if there is no awards to give it to the users then do nothing
            if (awards.find(a => a.IsAwarded === false) === undefined)
                return false;

            AwardsScript._resetWinnerNameAndCup();

            // preventing search for another user
            $(".select-user").attr("disabled", true);

            $(".selected-user-name")
                .removeClass("hvr-wobble-to-top-right visible")

            // show loader
            AwardsScript._showLoader();

            //reset the counter
            AwardsScript._resetCounter();

            var data = {
                order: AwardsScript.nextOrder,
                awardId: AwardsScript._getAwardByOrder(AwardsScript.nextOrder)
            };
            $.post(selectUserUrl, data)
                .done(function (result) {
                    // hide loader
                    AwardsScript._hideLoader();

                    if (result.Status) {
                        if (result.User !== undefined) {
                            var userId = result.User.Id;

                            // start counter
                            AwardsScript._resetCounter(userId);

                            // set the selected user 
                            AwardsScript._runSetTimeOut(result.User);
                        }
                    } else {
                        $.toaster(result.Message, 'خطأ', 'danger');
                    }
                }).fail(function () {
                    $.toaster("من فضلك اعد تحميل الصفحة", 'خطأ', 'danger');
                });
        })
    },

    _resetCounter: function (value = 0) {
        $(".incremental-counter").text("")
            .attr("data-value", value)
            .incrementalCounter(AwardsScript.counterOptions);
    },

    _showLoader: function () {
        var div = "<div class='loading-icon'></div><div class='spinner'> <div class='rect1'></div> <div class='rect2'></div> <div class='rect3'></div> <div class='rect4'></div> <div class='rect5'></div></div>";
        $('body').append(div);
    },

    _hideLoader: function () {
        $('.loading-icon, .spinner').remove();
    },

    _runSetTimeOut: function (user) {
        var interval = setInterval(function () {
            var num = "";
            // check if the counter reach the id of user to stop the time out function and render values in Html
            $.each($(".incremental-counter .num"), function (i, digit) {
                num += $(digit).text();
            });

            if (parseInt(num) === user.Id) {
                clearInterval(interval);
                AwardsScript._setTheSelectedUser(user);
                AwardsScript._updateAwards();
                AwardsScript._setUnWardedNextOrder();
                AwardsScript._setTheAwardName();
                // validate last award
                AwardsScript._validateLastAward();
            }
        }, 500);
    },

    _setTheSelectedUser: function (user) {
        // mark the user list item as selected user
        $(`[data-order=${this.nextOrder}].user`).addClass("selected")
            .find(".user-name")
            .addClass("hvr-wobble-to-top-right")
            .text(user.Name);

        $(".selected-user-name")
            .addClass("hvr-wobble-to-top-right visible")
            .text(user.Name);

        $(".cup-image img").removeClass("hidden")
            .addClass("hvr-wobble-to-top-right visible");

        // allowing search for another user
        $(".select-user").attr("disabled", false);
    },

    _setTheAwardName: function () {
        var awardwinner = awards.find(a => a.Award.Order === AwardsScript.nextOrder);
        if (awardwinner !== undefined) {
            $(".awards-page .award-name").text(awardwinner.Award.Name);
        }
    },

    _validateLastAward: function () {
        if (awards.find(a => a.IsAwarded === false) === undefined) {
            $(".select-user").remove();
        } else {
            $(".select-user").removeClass("hidden");
        }
    },

    _resetWinnerNameAndCup: function () {
        $(".selected-user-name").removeClass("hvr-wobble-to-top-right visible");
        $(".cup-image img").addClass("hidden")
            .removeClass("hvr-wobble-to-top-right visible");
    },

    _setUnWardedNextOrder: function () {
        this.nextOrder = Math.min.apply(Math, awards.filter(a => a.IsAwarded === false).map(a => a.Award.Order));
    },

    _getAwardByOrder: function (order) {
        var award = awards.find(a => a.Award.Order === order);
        if (award === undefined) return;

        return award.Award.Id;
    },

    _updateAwards: function () {
        var index = awards.findIndex(a => a.Award.Order === AwardsScript.nextOrder);
        awards[index]["IsAwarded"] = true;
    }
}