﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AwardsApp.Startup))]
namespace AwardsApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
