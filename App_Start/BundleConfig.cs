﻿using System.Web.Optimization;

namespace AwardsApp
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js",
                        "~/Scripts/jquery.incremental-counter.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js",
                      "~/Content/datatables/datatables.min.js",
                      "~/Content/bootbox/bootbox.min.js",
                      "~/Content/bootstrap-toastr/toastr.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/Awards").Include(
                      "~/Scripts/Awards.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap-rtl.min.css",
                      "~/Content/font-awesome/css/font-awesome.min.css",
                      "~/Content/jquery.incremental-counter.css",
                      "~/Content/hover-min.css",
                      "~/Content/datatables/datatables.min.css",
                      "~/Content/bootstrap-toastr/toastr-rtl.min.css",
                      "~/Content/site.css"));
        }
    }
}
